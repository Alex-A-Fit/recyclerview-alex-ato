package com.example.recyclerview

sealed class Resource(response: List<Int>?, message: String?){

    data class Success(val data: List<Int>) : Resource(data, null)

    object Loading: Resource(null, null)
}