package com.example.recyclerview.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.recyclerview.Resource
import com.example.recyclerview.model.MainRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {
    val repo by lazy { MainRepo }

    //variables to view
//    private var _countryList: MutableLiveData<Resource>? = null
//    val countryList: LiveData<Resource> get() = _countryList!!

    private var _rngColor: MutableLiveData<Resource> = MutableLiveData(Resource.Loading)
    val rngColor: LiveData<Resource> get() = _rngColor

    fun getRngColorList() {
        viewModelScope.launch(Dispatchers.Main) {
            val response = repo.getRngColorList()
        _rngColor.value = response
        }
    }
//    fun getCountryList() {
//        viewModelScope.launch {
//            val jsonString = repo.getJsonCountries()
//            val countries: Resource = Json.decodeFromString(jsonString)
//            _countryList?.value = countries
//
//        }
//    }
}