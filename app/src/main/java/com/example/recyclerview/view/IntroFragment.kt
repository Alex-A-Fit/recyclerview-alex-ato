package com.example.recyclerview.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerview.Resource
import com.example.recyclerview.databinding.FragmentIntroBinding
import com.example.recyclerview.view.adapter.adapter
import com.example.recyclerview.viewmodel.MainViewModel
import kotlinx.serialization.builtins.serializer

class IntroFragment : Fragment() {
    private var _binding: FragmentIntroBinding? = null
    private val binding: FragmentIntroBinding get() = _binding!!

    private val viewmodel by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentIntroBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitListener()
    }

    fun onInitObserver() = with(viewmodel){
        rngColor.observe(viewLifecycleOwner) { countries ->
            when(countries){
                is Resource.Success -> {
                    binding.recycleView.layoutManager = LinearLayoutManager(requireContext())
                    binding.recycleView.adapter = adapter().apply {
                        giveData(countries.data)
                    }
                }
            }
        }
    }
    fun onInitListener() = with(binding) {
        button.setOnClickListener {
            viewmodel.getRngColorList()
            onInitObserver()
        }
    }

}