package com.example.recyclerview.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.databinding.ListItemBinding

class adapter: RecyclerView.Adapter<adapter.viewHolder>() {
    private lateinit var data: List<Int>

    class viewHolder(private val binding:ListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun apply(item:Int) {
            binding.listName.setBackgroundColor(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false )
        return viewHolder(binding)
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val item = data[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }
    fun giveData(data: List<Int>){
        this.data = data
    }
}